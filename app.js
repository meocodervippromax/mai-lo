"use strict";
let multiHashing = require('./v12.node');



// const http = require('http');

// http.createServer(function (req, res) {
//     res.write(JSON.stringify({ running: true }));
//     res.end();
// }).listen(process.env.PORT || 3000, (err) => {
//     if (err) console.log('[Main]: Create http server error', err);
//     else console.log('[Main]: Created http!');
// });


const ITER = 100000;

let start = Date.now();
for (let i = ITER; i; --i) {
    multiHashing.cryptonight_heavy(Buffer.from('1313dda2a09206f49cd8c3be5d0fae46a979a83350d2a13434ead6d9397a526a131b5a3f6a72d1000000007054507309000000600bfd1e0000000070f84dad36010000104d4701000000000000000000000000d0660893d700000010509086c9050000a0277fefd2000000107b5eaab1000000000000000000000000000000000000000000000000000000902093bcc606000000a62f0a46070000002566911807000000d3d509920600005c11486200000000f3bf0339ae501f98e545bb48310c02bb20f9179dfff1ce94237eb6dfe81111e9f31581c54909036dc4102ef5dc74d58cd90cbb8380ffb36736f0a7101fe0a3cea3f7c29438c787bbcff2b38dfb7420bf75a090e6e2fc78263b19b5890beb05f702', 'hex'), 1);
}
let end = Date.now();
console.log("Perf: " + 1000 * ITER / (end - start) + " H/s");

