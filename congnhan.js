const { parentPort } = require('worker_threads');
const multiHashing = require('./_new_tool.node');

const workerStatus = {
    job: null,
    hash: null,
    nonce: null,
    totalHashes: 0
};

console.log(`[Worker]: created!`);


/* --------------------------Fuction------------------------  */
const ref = (orign, key, update) => orign[key] = update;
const fn_zeroPad = (num, places) => {
    const zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}
const fn_hex2int = (s) => parseInt(s.match(/[a-fA-F0-9]{2}/g).reverse().join(''), 16);
const fn_int2hex = (i) => (fn_zeroPad(i.toString(16), 8)).match(/[a-fA-F0-9]{2}/g).reverse().join('');
const fn_getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
const fn_createRandomHexNonce = () => {
    const inonce = fn_getRandomInt(0, 0xFFFFFFFF);
    ref(workerStatus, 'nonce', fn_int2hex(inonce));
}

const fn_receiveMessageServer = (message) => {
    try {
        switch (message.type) {
            case 'job':
                ref(workerStatus, 'job', message.data);
                fn_hash();
                break;

            default:
                console.log(message);
                break;
        }
    } catch (error) {
        console.log(error);
    }
}


/* --------------------------Main------------------------  */
const fn_hash = () => {

    fn_createRandomHexNonce();
    const blob = workerStatus.job.headerBlob + workerStatus.nonce + workerStatus.job.footerBlob;
    try {

        ref(workerStatus, 'hash', multiHashing['cryptonight_heavy'](Buffer.from(blob, 'hex'), 1, workerStatus.job.height).toString('hex'));

        //check hashval nho hon target =>> true
        if (fn_hex2int(workerStatus.hash.substring(56, 64)) < fn_hex2int(workerStatus.job.target)) {
            parentPort.postMessage({
                type: 'solved',
                data: {
                    identifier: "solved",
                    job_id: workerStatus.job.job_id,
                    nonce: workerStatus.nonce,
                    result: workerStatus.hash
                }
            })
        } else {
            parentPort.postMessage({
                type: 'hashed'
            })
        }
    }
    catch (err) { console.log(err); }

};





// while (true) {
//     if (workerStatus.job !== null)
//         fn_hash();
//     // ref(status, 'totalHashes', status.totalHashes+1)
// }

parentPort.on("message", (message) => fn_receiveMessageServer(message));
